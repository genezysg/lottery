
package app


import (
	"errors"
	"math/rand"
	"time"
	"sort"
)
type Lottery struct {
	rangeMin, rangeMax int
	repeat bool
	repeatNumbersMax int
	NumberSorted []int
	gameNumbers int
}

func fillLottery(rangeMin, rangeMax int) ([]int, error) {
	if (rangeMax <= rangeMin) {
		return nil, errors.New("wrong range")
	}
	filledLottery := make([]int, (rangeMax - rangeMin)+1 )
	initialValue := rangeMin
	for i, _:= range filledLottery {
		filledLottery[i] = initialValue
		initialValue++
	}
	return filledLottery, nil
}

func remove(s []int, i int) []int {
    s[len(s)-1], s[i] = s[i], s[len(s)-1]
    return s[:len(s)-1]
}

func (l *Lottery) Sort (lottery []int) {
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	if (len(l.NumberSorted) == 0){
		for i:= 0; i < l.gameNumbers; i++ {
			sorted:= r.Intn(len(lottery))
			l.NumberSorted = append(l.NumberSorted, lottery[sorted])
			lottery = remove(lottery, sorted)
		} 
	}
	sort.Ints(l.NumberSorted)
}



