package app

import (
	"testing"
	"github.com/stretchr/testify/assert"
	"sort"
)

func results(a string) []int {
	switch (a) {
	case "LOTTERYFILLED1TO15":
		return []int{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15}
	break;	
	} 
	return nil
} 

func Repeated() []int {
	return []int{1,1,1,3,2,3}
} 
func NotRepeated() []int {
	return []int{1,2,3,4,5,6}
}


func TestFillLottery(t *testing.T) {
	lottery, err := fillLottery(1,15)
	if (err != nil) {
		assert.Error(t, err)
	}
	assert := assert.New(t)
	
	assert.Equal(lottery,results("LOTTERYFILLED1TO15"))
}

func checkRepetition(r []int) bool {
	reps := make(map[int]bool)
	for _, element := range r {
		reps[element] = true
	}
	return !(len(reps) == len(r))
}

func checkInRange(r []int, rangeMin, rangeMax int) bool {
	for _, element := range r {
		if (element < rangeMin) {
			return false
		}
		if (element > rangeMax) {
			return false
		}
	}
	return true
}

func TestNotRepeated(t *testing.T) {
	lottery :=  &Lottery{ NumberSorted: make([]int,0), gameNumbers:6}
	resultLottery,_ := fillLottery(1,60)
	lottery.Sort(resultLottery)
	assert := assert.New(t)
	assert.Equal(false,checkRepetition(NotRepeated()))
	assert.Equal(false,checkRepetition(lottery.NumberSorted))


}

func TestRepeated(t *testing.T) {
	lottery :=  &Lottery{ NumberSorted: make([]int,0), gameNumbers:6}
	resultLottery,_ := fillLottery(1,60)
	lottery.Sort(resultLottery)
	assert := assert.New(t)
	assert.Equal(true,checkRepetition(Repeated()))

}


func TestSort(t *testing.T) {
	assert := assert.New(t)
	lottery :=  &Lottery{ NumberSorted: make([]int,0), gameNumbers:6}
	resultLottery,_ := fillLottery(1,60)
	lottery.Sort(resultLottery)
	assert.Equal(true,sort.IntsAreSorted(lottery.NumberSorted))	
}

func TestInRange(t *testing.T) {
	assert := assert.New(t)
	lottery :=  &Lottery{ NumberSorted: make([]int,0), gameNumbers:6}
	resultLottery,_ := fillLottery(1,60)
	lottery.Sort(resultLottery)
	assert.Equal(true, checkInRange(lottery.NumberSorted,1,60))	
	assert.Equal(false, checkInRange([]int{1,61},1,60))
	assert.Equal(false, checkInRange([]int{0,10,6},1,60))
	assert.Equal(true, checkInRange(Repeated(),1,60))	
}